.DEFAULT_GOAL := all

all: backend frontend

frontend: clean
	cd client && yarn install && yarn build
	mv client/build ./build/www

backend:
	go build -v -o ./build/booqmarq ./server

clean:
	rm -rf ./build/www
	go clean

default: backend

.PHONY: all clean
