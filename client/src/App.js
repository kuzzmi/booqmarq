import React, { Component } from 'react';
import './App.css';
import BookmarksList from './BookmarksList.js';
import BookmarkForm from './BookmarkForm.js';
import ErrorView from './Error.js';
import { indexBy } from './utils.js';

import API from './api.js';

class App extends Component {
    state = {
        bookmarkFormVisible: false,
        bookmarkToEdit: null,

        allBookmarks: [],
        allLabels: {},

        visibleBookmarks: [],
        selectedTag: null,

        error: null,
    };

    constructor(props) {
        super(props);

        this.handleSubmit = ({ bookmark, labels }) => {
            const isUpdate = !!bookmark.id;
            const request =
                bookmark.id ?
                    API.putBookmark(bookmark, labels) :
                    API.postBookmark(bookmark, labels);

            request.then(({ bookmark, labels }) => {
                const labels_ = indexBy('id', labels);
                const labelIDs = labels.map(l => l.id);

                this.setState(state => {
                    const allBookmarks =
                        isUpdate ?
                            state.allBookmarks.map(b => {
                                if (b.bookmark.id === bookmark.id) {
                                    return { bookmark, labelIDs };
                                } else {
                                    return b;
                                }
                            }) :
                            [
                                { bookmark, labelIDs },
                                ...state.allBookmarks,
                            ];

                    return {
                        ...state,

                        bookmarkFormVisible: false,
                        bookmarkToEdit: null,

                        allBookmarks,
                        allLabels: {
                            ...this.state.allLabels,
                            ...labels_,
                        },
                    };
                });
            })
            .catch(err => {
                this.setState(state => ({
                    ...state,
                    error: err,
                }));
            });
        };

        this.reloadBookmarks = () => {
            API.getInitData()
                .then(({ bookmarks, labels }) => {
                    const labels_ = labels.map(l => {
                        const count = bookmarks.filter(({ labelIDs }) => {
                            return labelIDs.indexOf(l.id) > -1;
                        }).length;

                        l.count = count;
                        return l;
                    });
                    this.setState(state => ({
                        ...state,
                        allBookmarks: bookmarks.reverse(),
                        visibleBookmarks: bookmarks.reverse(),
                        allLabels: indexBy('id', labels_),
                    }));
                })
                .catch(err => {
                    this.setState(state => ({
                        ...state,
                        error: err,
                    }));
                });
        };

        this.deleteBookmark = bookmarkId => {
            API.deleteBookmark(bookmarkId).then(() => {
                this.setState(state => ({
                    ...state,
                    allBookmarks: state.allBookmarks.filter(({ bookmark }) => {
                        return bookmark.id !== bookmarkId;
                    }),
                    visibleBookmarks: state.visibleBookmarks.filter(({ bookmark }) => {
                        return bookmark.id !== bookmarkId;
                    }),
                }));
            });
        };

        this.editBookmark = (bookmark, labelIDs) => {
            const labels = labelIDs.map(id => this.state.allLabels[id].title).join(', ');
            this.setState(state => ({
                ...state,
                bookmarkFormVisible: true,
                bookmarkToEdit: {
                    bookmark,
                    labels,
                },
            }));
        }

        this.addNewBookmark = () => {
            this.setState(state => ({
                ...state,
                bookmarkFormVisible: !state.bookmarkFormVisible,
                bookmarkToEdit: null,
            }));
        };

        this.updateSearch = event => {
            const { value } = event.target;
            if (value.length === 0) {
                this.setState(state => ({
                    ...state,
                    visibleBookmarks: state.allBookmarks,
                }));
            } else {
                const value_ = value.toLowerCase();
                this.setState(state => ({
                    ...state,
                    visibleBookmarks: state.allBookmarks.filter(({ bookmark }) => {
                        const { title, description } = bookmark;

                        if (title.toLowerCase().indexOf(value_) > -1 ||
                            description.toLowerCase().indexOf(value_) > -1) {
                            return true;
                        } else {
                            return false;
                        }
                    }),
                }));
            }
        };

        this.filterByLabel = labelID => {
            if (!labelID) {
                this.setState(state => ({
                    ...state,
                    selectedTag: null,
                    visibleBookmarks: state.allBookmarks,
                }));
            } else {
                this.setState(state => ({
                    ...state,
                    selectedTag: state.allLabels[labelID].title,
                    visibleBookmarks: state.allBookmarks.filter(({ labelIDs }) => {
                        return labelIDs.indexOf(labelID) > -1;
                    }),
                }));
            }
        };
    }

    componentWillMount() {
        this.reloadBookmarks();
    }

    render() {
        const { bookmarkFormVisible, selectedTag, allLabels } = this.state;

        return (
            <div className="App">
                <div className="toolbar">
                    <div>
                        <button
                            className="positive"
                            onClick={ this.addNewBookmark }>
                            { !!bookmarkFormVisible ? 'Hide form' : 'Add new' }
                        </button>
                    </div>
                    <input type="search" placeholder="Search" onChange={ this.updateSearch } />
                </div>

                <div className="container">
                    <div>
                        <ul className="tagsList">
                        {
                            Object.keys(allLabels).map(id => (
                                <li key={ id }>
                                    <a onClick={ () => this.filterByLabel(+id) }>
                                        { allLabels[id].title || 'unlabeled' }
                                    </a>
                                    <span> ({ allLabels[id].count })</span>
                                </li>
                            ))
                        }
                        </ul>
                    </div>
                    <div>
                        {
                            (selectedTag || selectedTag === '') &&
                                <div className="selectedTag">
                                    <span>Selected tag: </span>
                                    <a onClick={ () => this.filterByLabel(null) }>
                                        <b>{ selectedTag || 'unlabeled' }</b> x
                                    </a>
                                </div>
                        }

                        <BookmarkForm
                            visible={ this.state.bookmarkFormVisible }
                            bookmark={ this.state.bookmarkToEdit }
                            handleSubmit={ this.handleSubmit }
                        />

                        <BookmarksList
                            bookmarks={ this.state.visibleBookmarks }
                            labels={ this.state.allLabels }
                            editBookmark={ this.editBookmark }
                            deleteBookmark={ this.deleteBookmark }
                            filterByLabel={ this.filterByLabel }
                        />
                    </div>
                </div>

                <ErrorView error={ this.state.error } />
            </div>
        );
    }
}

export default App;
