import React, { Component } from 'react';

export default class extends Component {
    state = {
        id: null,
        title: '',
        description: '',
        url: '',
        labels: '',
    };

    constructor(props) {
        super(props);

        this.handleChangeField = event => {
            const { name, value } = event.target;

            this.setState(state => ({
                ...state,
                [name]: value,
            }));
        };

        this.handleSubmit = event => {
            event.preventDefault();

            const bookmark = {
                id: this.state.id,
                title: this.state.title,
                description: this.state.description,
                url: this.state.url,
            };

            const labels = this.state.labels.split(',').map(a => a.trim());

            props.handleSubmit({
                bookmark,
                labels,
            });
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.bookmark) {
            const { bookmark, labels } = nextProps.bookmark;
            this.setState(state => ({
                id: bookmark.id,
                title: bookmark.title,
                description: bookmark.description,
                url: bookmark.url,
                labels,
            }));
        } else {
            this.setState(state => ({
                id: null,
                title: '',
                description: '',
                url: '',
                labels: '',
            }));
        }
    }

    render() {
        const { visible } = this.props;

        if (!visible) {
            return null;
        }

        return (
            <form onSubmit={ this.handleSubmit }>
                <div>
                    <input
                        onChange={ this.handleChangeField }
                        value={ this.state.title }
                        name="title"
                        placeholder="Title"
                    />
                </div>
                <div>
                    <input
                        onChange={ this.handleChangeField }
                        value={ this.state.labels }
                        name="labels"
                        placeholder="Labels"
                    />
                </div>
                <div>
                    <textarea
                        rows="3"
                        onChange={ this.handleChangeField }
                        value={ this.state.description }
                        name="description"
                        placeholder="Description"
                    />
                </div>
                <div>
                    <input
                        onChange={ this.handleChangeField }
                        value={ this.state.url }
                        name="url"
                        placeholder="URL"
                        required
                        pattern="https?://.+"
                    />
                </div>
                <div>
                    <input type="submit" value={
                        this.state.id ? 'Update' : 'Add'
                    }/>
                </div>
            </form>
        );
    }
}
