import React from 'react';
import { formatUnixDate, getHostname } from './utils.js';
import Favicon from './Favicon.js';
import './BookmarksList.css';

const Labels = ({ labels, ids, filter }) => (
    ids.map((id, i) => (
        <a onClick={ () => filter(id) } key={ i }>
            <small>
                <b>
                    { labels[id].title || 'unlabeled' }
                    { i < ids.length - 1 && ', ' }
                </b>
            </small>
        </a>
    ))
);

export default function({
    bookmarks,
    labels,
    filterByLabel,
    editBookmark,
    deleteBookmark,
}) {
    return (
        <table className="bookmarks">
            <tbody>
            {
                bookmarks.map(({ bookmark, labelIDs }, index) => (
                    <tr key={ bookmark.id }>
                        <td>
                            <div className="title">
                                <Favicon className="favicon" url={ bookmark.url } />
                                <a target="_blank" tabIndex={ index } href={ bookmark.url }>
                                    <b>{ bookmark.title }</b>
                                </a>
                                <span className="spacer">&middot;</span>
                                <span className="subtle">
                                    { getHostname(bookmark.url) }
                                </span>
                            </div>
                            <div className="details">
                                {
                                    bookmark.description &&
                                    <p className="description">
                                        { bookmark.description }
                                    </p>
                                }
                                <Labels labels={ labels } ids={ labelIDs } filter={ filterByLabel } />
                            </div>
                        </td>
                        <td className="date">
                            <div>
                                { formatUnixDate(bookmark.created_at) }
                            </div>
                            <div>
                                <a onClick={ () => editBookmark(bookmark, labelIDs) }>
                                    edit
                                </a>
                                <span className="spacer">&middot;</span>
                                <a onClick={ () => deleteBookmark(bookmark.id) }>
                                    remove
                                </a>
                            </div>
                        </td>
                    </tr>
                ))
            }
            </tbody>
        </table>
    );
}
