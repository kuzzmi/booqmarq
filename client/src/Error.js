import React, { Component } from 'react';
import './Error.css';

export default class extends Component {
    state = {
        visible: false,
        expanded: false,
    };

    constructor(props) {
        super(props);

        this.hide = () => {
            this.setState(state => ({
                ...state,
                visible: false,
            }));
        };

        this.toggleExpanded = event => {
            this.setState(state => ({
                ...state,
                expanded: !state.expanded,
            }));
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.error !== this.props.error) {
            this.setState(state => ({
                ...state,
                visible: true,
                expanded: false,
            }));
        };
    }

    render() {
        const { error } = this.props;

        if (!this.state.visible) {
            return null;
        }

        return (
            <div className="error-container">
                <div className="title-container">
                    <span>Failure</span>
                    <button onClick={ this.hide }>
                        x
                    </button>
                </div>
                {
                    this.state.expanded &&
                    <pre>
                        <code>
                            { JSON.stringify(error) }
                        </code>
                    </pre>
                }
                <div className="details-container">
                    <button onClick={ this.toggleExpanded }>
                        <span className="details-toggle">
                            { this.state.expanded ? 'hide' : 'show' } details
                        </span>
                    </button>
                </div>
            </div>
        );
    }
}
