const API_BASE =
    process.env.NODE_ENV === 'production' ?
        '//bq.kuzzmi.com/api' :
        'http://localhost:8080';

export default {
    postBookmark: (bookmark, labels) => {
        return fetch(`${ API_BASE }/bookmarks/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                bookmark,
                labels,
            }),
        }).then(data => data.json());
    },
    putBookmark: (bookmark, labels) => {
        return fetch(`${ API_BASE }/bookmarks/${ bookmark.id }`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                bookmark,
                labels,
            }),
        }).then(data => data.json());
    },
    deleteBookmark: bookmarkId => {
        return fetch(`${ API_BASE }/bookmarks/${ bookmarkId }`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(data => data.json());
    },
    getInitData: () => {
        return fetch(`${ API_BASE }/init/`).then(data => data.json())
    },
};

