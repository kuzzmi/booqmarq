export const leftpad = (str, len, ch) => {
    str = str + '';
    len = len - str.length;
    if (len <= 0) return str;
    if (!ch && ch !== 0) ch = ' ';
    ch = ch + '';
    var pad = '';
    while (true) {
        if (len & 1) pad += ch;
        len >>= 1;
        if (len) ch += ch;
        else break;
    }
    return pad + str;
};

export const formatUnixDate = date => {
    // We'll be getting all info about a date here
    // unix date is seconds, we need ms
    const date_ = new Date(date * 1000);

    const y = date_.getFullYear();
    const m = leftpad(date_.getMonth() + 1, 2, '0');
    const d = leftpad(date_.getDate(), 2, '0');
    const hh = leftpad(date_.getHours(), 2, '0');
    const mm = leftpad(date_.getMinutes(), 2, '0');

    return `${y}-${m}-${d} ${hh}:${mm}`;
};

export const getHostname = url => {
    const link = document.createElement('a');
    link.href = url;
    return link.host;
};

export const indexBy = (prop, arr) =>
    arr.reduce((acc, cur) => ({
        ...acc,
        [cur[prop]]: cur,
    }), {});
