package main

import (
	"database/sql"
	"fmt"
	"time"

	"log"

	_ "github.com/mattn/go-sqlite3"
)

type DB struct {
	db *sql.DB
}

func NewDB(dbFile string) (*DB, error) {
	db, err := sql.Open("sqlite3", dbFile)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	tablesStmt := `
		CREATE TABLE IF NOT EXISTS bookmark (
			id INTEGER PRIMARY KEY NOT NULL,
			title TEXT NOT NULL,
			description TEXT NULL,
			url TEXT NOT NULL,
			created_at INTEGER NOT NULL,
			UNIQUE (url COLLATE NOCASE)
		) ;

		CREATE TABLE IF NOT EXISTS label (
			id INTEGER PRIMARY KEY NOT NULL,
			title TEXT NOT NULL,
			created_at INTEGER NOT NULL,
			UNIQUE (title COLLATE NOCASE)
		) ;

		CREATE TABLE IF NOT EXISTS bookmarkLabel (
			id INTEGER PRIMARY KEY NOT NULL,
			bookmark_id INT NOT NULL,
			label_id INT NOT NULL,
			FOREIGN KEY(bookmark_id) REFERENCES bookmark(id),
			FOREIGN KEY(label_id) REFERENCES label(id),
			UNIQUE (bookmark_id, label_id)
		)
	`

	_, err = db.Exec(tablesStmt)
	if err != nil {
		return nil, err
	}

	return &DB{db: db}, nil
}

// --- Models

type Bookmark struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	URL         string `json:"url"`
	CreatedAt   int64  `json:"created_at"`
}

type BookmarkExt struct {
	Bookmark *Bookmark `json:"bookmark"`
	LabelIDs []int     `json:"labelIDs"`
}

type BookmarkInPayload struct {
	Bookmark *Bookmark `json:"bookmark"`
	Labels   []string  `json:"labels"`
}

func (db *DB) CreateBookmark(bookmark *Bookmark) (*Bookmark, error) {
	statement, err := db.db.Prepare(`
		INSERT INTO bookmark (title, description, url, created_at)
			VALUES (?, ?, ?, ?)
	`)
	if err != nil {
		return nil, err
	}

	now := time.Now().Unix()

	res, err := statement.Exec(
		bookmark.Title,
		bookmark.Description,
		bookmark.URL,
		now,
	)
	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()

	bookmark.ID = int(id)
	bookmark.CreatedAt = now

	return bookmark, nil
}

func (db *DB) UpdateBookmark(bookmark *Bookmark) (*Bookmark, error) {
	statement, err := db.db.Prepare(`
		UPDATE bookmark
			SET
				title=?,
				description=?,
				url=?
			WHERE id=? ;
	`)
	if err != nil {
		fmt.Printf("Error updating bookmark", err)
		return nil, err
	}

	_, err = statement.Exec(
		bookmark.Title,
		bookmark.Description,
		bookmark.URL,
		bookmark.ID,
	)
	if err != nil {
		return nil, err
	}

	return bookmark, nil
}

func (db *DB) GetBookmarkByID(id int) (*Bookmark, error) {
	row := db.db.QueryRow(`
		SELECT id, title, description, url, created_at
		FROM bookmark
		WHERE id = ?
	`, id)

	var bookmark Bookmark

	err := row.Scan(
		&bookmark.ID,
		&bookmark.Title,
		&bookmark.Description,
		&bookmark.URL,
		&bookmark.CreatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &bookmark, nil
}

func (db *DB) DeleteBookmarkByID(id int) error {
	statement, err := db.db.Prepare(`
		DELETE FROM bookmark WHERE id = ?
	`)
	if err != nil {
		return err
	}

	_, err = statement.Exec(id)
	if err != nil {
		return err
	}

	statement, err = db.db.Prepare(`
		DELETE FROM bookmarkLabel
		WHERE bookmark_id = ?
	`)
	if err != nil {
		return err
	}

	_, err = statement.Exec(id)
	if err != nil {
		return err
	}

	return nil
}

func (db *DB) GetAllBookmarks() ([]*BookmarkExt, error) {
	bookmarks := []*BookmarkExt{}

	rows, err := db.db.Query(`
		SELECT id, title, description, url, created_at
		FROM bookmark
	`)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var bookmark Bookmark
		err := rows.Scan(
			&bookmark.ID,
			&bookmark.Title,
			&bookmark.Description,
			&bookmark.URL,
			&bookmark.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		labels, err := db.GetLabelIdsByBookmark(bookmark.ID)
		if err != nil {
			fmt.Printf("Error getting labels for bookmark", err)
			return nil, err
		}
		bookmarkExt := &BookmarkExt{
			Bookmark: &bookmark,
			LabelIDs: labels,
		}
		bookmarks = append(
			bookmarks,
			bookmarkExt,
		)
	}

	err = rows.Err()

	if err != nil {
		return nil, err
	}

	return bookmarks, nil
}

// ---------------------
// Label

type Label struct {
	ID        int    `json:"id"`
	Title     string `json:"title"`
	CreatedAt int64  `json:"created_at"`
}

func (db *DB) UpsertLabel(labelTitle string, bookmarkID int) (*Label, error) {
	var label *Label

	label, err := db.GetLabelByTitle(labelTitle)
	if err == nil && label != nil {
		db.CreateBookmarkLabel(label.ID, bookmarkID)

		return label, nil
	}

	statement, err := db.db.Prepare(`
		INSERT OR IGNORE INTO label (title, created_at)
			VALUES (?, ?)
	`)
	if err != nil {
		return nil, err
	}

	now := time.Now().Unix()

	res, err := statement.Exec(
		labelTitle,
		now,
	)
	if err != nil {
		return nil, err
	}

	affected, err := res.RowsAffected()
	if err != nil {
		return nil, err
	}

	if affected == 0 {

	} else {
		id, err := res.LastInsertId()

		label = &Label{
			ID:        int(id),
			Title:     labelTitle,
			CreatedAt: now,
		}

		fmt.Println("Label ID", label.ID, "BookmarkID", bookmarkID)
		err = db.CreateBookmarkLabel(label.ID, bookmarkID)

		if err != nil {
			return nil, err
		}
	}

	return label, nil
}

func (db *DB) UpsertLabels(labelTitles []string, bookmarkID int) ([]*Label, error) {
	labels := []*Label{}

	fmt.Println("Creating labels", labelTitles)

	for _, labelTitle := range labelTitles {
		label, err := db.UpsertLabel(labelTitle, bookmarkID)
		if err != nil {
			return nil, err
		}
		labels = append(
			labels,
			label,
		)
	}

	return labels, nil
}

func (db *DB) GetLabelByTitle(title string) (*Label, error) {
	row := db.db.QueryRow(`
		SELECT id, title, created_at
		FROM label
		WHERE title = ? COLLATE NOCASE
	`, title)

	var label Label

	err := row.Scan(
		&label.ID,
		&label.Title,
		&label.CreatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &label, nil
}

func (db *DB) CreateBookmarkLabel(labelID int, bookmarkID int) error {
	statement, err := db.db.Prepare(`
		INSERT INTO bookmarkLabel (label_id, bookmark_id)
			VALUES (?, ?)
	`)
	if err != nil {
		return err
	}

	_, err = statement.Exec(
		labelID,
		bookmarkID,
	)
	if err != nil {
		return err
	}

	return nil
}

func (db *DB) GetLabelIdsByBookmark(bookmarkID int) ([]int, error) {
	ids := []int{}

	rows, err := db.db.Query(`
		SELECT label_id
		FROM bookmarkLabel
		WHERE bookmark_id = ?
	`, bookmarkID)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var id int
		err := rows.Scan(&id)
		if err != nil {
			return nil, err
		}
		ids = append(
			ids,
			id,
		)
	}

	err = rows.Err()

	if err != nil {
		return nil, err
	}

	return ids, nil
}

func (db *DB) GetAllLabels() ([]*Label, error) {
	labels := []*Label{}

	rows, err := db.db.Query(`
		SELECT id, title, created_at
		FROM label
	`)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var label Label
		err := rows.Scan(
			&label.ID,
			&label.Title,
			&label.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		labels = append(
			labels,
			&label,
		)
	}

	err = rows.Err()

	if err != nil {
		return nil, err
	}

	return labels, nil
}
