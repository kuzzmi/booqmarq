package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"

	"github.com/PuerkitoBio/goquery"
	"net/http"
)

func GetInitData(c *gin.Context) {
	bookmarks, err := Ctx.DB.GetAllBookmarks()
	if err != nil {
		handleError(c, err)
		fmt.Printf("Error getting bookmarks")
		return
	}

	labels, err := Ctx.DB.GetAllLabels()
	if err != nil {
		handleError(c, err)
		fmt.Printf("Error getting labels", err)
		return
	}

	c.JSON(200, gin.H{
		"bookmarks": bookmarks,
		"labels":    labels,
	})
}

func CreateBookmark(c *gin.Context) {
	json := &BookmarkInPayload{}

	err := c.BindJSON(&json)
	if err != nil {
		handleError(c, err)
		return
	}

	if json.Bookmark.Title == "" || json.Bookmark.Description == "" {
		resp, err := http.Get(json.Bookmark.URL)
		if err != nil {
			handleError(c, err)
			return
		}
		defer resp.Body.Close()

		var metaDescription string
		var pageTitle string

		doc, err := goquery.NewDocumentFromReader(resp.Body)
		if err != nil {
			handleError(c, err)
			return
		}

		pageTitle = doc.Find("title").Contents().Text()

		doc.Find("meta").Each(func(index int, item *goquery.Selection) {
			if item.AttrOr("name", "") == "description" {
				metaDescription = item.AttrOr("content", "")
			}
		})

		if json.Bookmark.Title == "" {
			json.Bookmark.Title = pageTitle
		}

		if json.Bookmark.Description == "" {
			json.Bookmark.Description = metaDescription
		}

	}

	bookmark, err := Ctx.DB.CreateBookmark(json.Bookmark)
	if err != nil {
		handleError(c, err)
		return
	}

	labels, err := Ctx.DB.UpsertLabels(json.Labels, bookmark.ID)
	if err != nil {
		handleError(c, err)
		fmt.Printf("Error saving labels", err)
		return
	}

	c.JSON(200, gin.H{
		"bookmark": bookmark,
		"labels":   labels,
	})
}

func UpdateBookmark(c *gin.Context) {
	json := &BookmarkInPayload{}

	err := c.BindJSON(&json)
	if err != nil {
		handleError(c, err)
		return
	}

	bookmark, err := Ctx.DB.UpdateBookmark(json.Bookmark)
	if err != nil {
		handleError(c, err)
		return
	}

	labels, err := Ctx.DB.UpsertLabels(json.Labels, bookmark.ID)
	if err != nil {
		handleError(c, err)
		fmt.Printf("Error saving labels", err)
		return
	}

	c.JSON(200, gin.H{
		"bookmark": bookmark,
		"labels":   labels,
	})
}

func DeleteBookmark(c *gin.Context) {
	bookmarkId, err := strconv.Atoi(c.Param("bookmark_id"))
	if err != nil {
		handleError(c, err)
		fmt.Printf("Error getting bookmark id", err)
		return
	}

	err = Ctx.DB.DeleteBookmarkByID(bookmarkId)
	if err != nil {
		handleError(c, err)
		fmt.Printf("Error deleting bookmark", err)
		return
	}

	handleOK(c)
}

// ---------------------
// -- Gin utility functions

func handleOK(c *gin.Context) {
	c.JSON(200, gin.H{
		"status": "OK",
	})
}

func handleError(c *gin.Context, err error) {
	c.JSON(500, gin.H{
		"status": "Failure",
		"error":  err,
	})
}
