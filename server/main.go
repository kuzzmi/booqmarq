package main

import (
	"github.com/gin-gonic/gin"
)

type Context struct {
	DB *DB
}

var Ctx Context

// Used for local development
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:3001")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func main() {
	dbFile := "./data.db"

	db, err := NewDB(dbFile)
	if err != nil {
		panic(err)
	}

	Ctx.DB = db

	r := gin.Default()

	r.Use(CORSMiddleware())

	r.GET("/init/", GetInitData)

	bookmarks := r.Group("/bookmarks")
	{
		bookmarks.POST("/", CreateBookmark)
		bookmarks.PUT("/:id", UpdateBookmark)
		bookmarks.DELETE("/:bookmark_id", DeleteBookmark)
	}

	r.Run()
}
